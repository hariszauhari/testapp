package codigo.id.apptest;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Codigo on 10/12/2017.
 */

public class QnockReceiver extends BroadcastReceiver {

    public QnockReceiver(Context context, String appsecret, String username, Class pushActivity) {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String title = "Qnock";
        String body = "notification";

        if (intent.getStringExtra("body")!=null) {
            body = intent.getStringExtra("body");
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(body)
                .setLights(Color.RED, 100, 100)
                .setVibrate(new long[]{0, 400, 250, 400})
                .setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, builder.build());

    }
}
