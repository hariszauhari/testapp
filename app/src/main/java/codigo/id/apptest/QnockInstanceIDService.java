package codigo.id.apptest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Codigo on 10/12/2017.
 */

public class QnockInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = codigo.id.apptest.QnockInstanceIDService.class.getSimpleName();

    //    FcmListener fcmListener;
//
    @Override
    public void onTokenRefresh() {
        //super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent("registrationComplete");
        registrationComplete.putExtra("token", refreshedToken);
        //LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        sendRegistrationToServer(refreshedToken);

        System.out.println("TOKEN FIREBASE : " + refreshedToken);
        Log.e(TAG, "token: " + refreshedToken);
//        Intent registrationComplete = new Intent("registrationComplete");
//        registrationComplete.putExtra("token", refreshedToken);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

        System.out.println("TOKEN FIREBASE : " + refreshedToken);

        Intent intent = new Intent();
        intent.putExtra("firebaseId", refreshedToken);
        intent.setAction("codigo.id.apptest.QnockReceiver");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        //setCallback(refreshedToken);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
//        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("ah_firebase", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();
    }

//    private void setCallback(String fcm) {
//        fcmListener.onFcmReceived(true, "", fcm);
//  }
}
