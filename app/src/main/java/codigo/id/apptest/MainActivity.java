package codigo.id.apptest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.List;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.controller.Qnock;
import id.codigo.qnocklib.controller.Qnock2;
import id.codigo.qnocklib.interfaces.CallbackInterface;
import id.codigo.qnocklib.interfaces.StatusInterface;
import id.codigo.qnocklib.interfaces.SubscribeInterface;
import id.codigo.qnocklib.interfaces.TokenInterface;
import id.codigo.qnocklib.interfaces.UnsubscribeInterface;
import id.codigo.qnocklib.model.CallbackAndroid;
import id.codigo.qnocklib.model.GetStatus;
import id.codigo.qnocklib.model.SubscribeChannel;
import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.qnocklib.model.UnsubscribeChannel;

/**
 * Created by Codigo on 10/12/2017.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    String firebaseId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseId = FirebaseInstanceId.getInstance().getToken();

        final Qnock2 qnock2 = new Qnock2();
        qnock2.getToken(new TokenInterface() {
            @Override
            public void onTokenGet(boolean status, String msg, TokenGenerate tokenGenerate) {
                System.out.println("Qnock Token ID: " + tokenGenerate.getDisplay_message());
                qnock2.getStatus(new StatusInterface() {
                    @Override
                    public void onStatusGet(boolean status, String msg, GetStatus getStatus) {
                        System.out.println("STATUS :" + getStatus.getStatus());
                    }
                }, tokenGenerate.getDisplay_message());
            }
        });

        String tokenSaved = qnock2.getTokenSaved();

        Log.d("tokensaved ", tokenSaved);

        List<String> list = new ArrayList<>();
        list.add("Sport");
        /*qnock2.subscribeChannel(new SubscribeInterface() {
            @Override
            public void onSubscribeGet(boolean status, String msg, SubscribeChannel subscribeChannel) {
                Log.d("subscribeTEST", msg);
               // Log.d("subscribeTEST", subscribeChannel.getMessage());
            }
        }, "fqsgX0FlYrc:APA91bEAQT6gQeEHov\n" +
                "IDRpdfOm_E5dZBRODIcyouFVS8G9\n" +
                "REvnzyoh8XyNGXDmnCpBDNACsdC\n" +
                "nC1OKzCQACBx32IUTL7FFhiV_IElO\n" +
                "c9TGGuvLPndU4ZrHxzjpAps6dRNefl", list, "Android", tokenSaved);*/
//        qnock2.unsubscribeChannel(new UnsubscribeInterface() {
//            @Override
//            public void onUnsubscribeGet(boolean status, String msg, UnsubscribeChannel unsubscribeChannel) {
//                Log.d("unsubscribeTEST", msg);
//            }
//        }, QnockConfig.appSecret, tokenSaved, list, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRfaWQiOiI5IiwiZXhwIjoxNTA3OTAzMjU2NDE0LCJpYXQiOjE1MDc4MTY4NTZ9.8J_Yf1ELCO88iHr9K4AaYvcF_wO2wWfybmQCX04Z770");
        qnock2.sendImpression(new CallbackInterface() {
            @Override
            public void onCallbackGet(boolean status, String msg, CallbackAndroid callbackObject) {
                System.out.println("SEND IMPRESSION:" + msg);
            }
        },QnockConfig.appSecret, QnockConfig.appId, Qnock2.getTokenSaved());
    }

    @Override
    public void onClick(View v) {

    }
}
