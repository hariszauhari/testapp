package id.codigo.qnocklib.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import id.codigo.qnocklib.config.Config;
import id.codigo.seedroid.helper.PreferenceHelper;

/**
 * Created by Codigo on 10/5/2017.
 */

public class Util {
    public static String getDateNow() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
}
