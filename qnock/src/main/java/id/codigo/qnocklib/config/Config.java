package id.codigo.qnocklib.config;

import android.util.Log;

/**
 * Created by Andini Rachmah on 8/1/2017.
 */

public class Config {
    static String environment;
    public static String URL_BASE = "";
    public static Config instance;

    public Config() {
    }

    public void setEnvironments(String environment) {
        if (environment.equals("1")) {
            URL_BASE = "http://push.qnock.netconnect.stg.codigo.id";
        }
        Log.e("ENVIRONMENT", URL_BASE);
    }

    public static final String URL_GET_TOKEN = URL_BASE + "/token";
    public static final String URL_GET_STATUS = URL_BASE + "/apps/status";
    public static final String URL_SEND_IMPRESSION = URL_BASE + "/callback/android";
    public static final String URL_SUBSCRIBE_CHANNEL = URL_BASE + "/user/subscribe";
    public static final String URL_UNSUBSCRIBE_CHANNEL = URL_BASE + "/user/unsubscribe";

    public static final String token1 = "token";
    public static final String tokenDate = "tokenDate";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;

    public static final String SHARED_PREF = "ah_firebase";

    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final String QNOCK_TOKEN = "id.codigo.qnocklib.TokenReceiver";

}
