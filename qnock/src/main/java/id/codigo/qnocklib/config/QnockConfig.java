package id.codigo.qnocklib.config;

import android.util.Base64;

import id.codigo.seedroid.configs.RestConfigs;

/**
 * Created by Codigo on 10/12/2017.
 */

public class QnockConfig {
    public static String appId = "";
    public static String appSecret = "";
    public static String URL_BASE = "http://push.qnock.netconnect.stg.codigo.id";
    public static String unix_Id = "";

    public QnockConfig(String appId, String appSecret){
        this.appId = appId;
        this.appSecret = appSecret;

        setRestConfig(this.appId, this.appSecret);
    }

    private void setRestConfig(String appId, String appSecret){
        RestConfigs.rootUrl = URL_BASE;
        String credentials = appId + ":" + appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        RestConfigs.defaultHeader.put("Authorization", auth);
        RestConfigs.defaultHeader.put("Content-Type", "application/json; charset=utf-8");
    }
}
