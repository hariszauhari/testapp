package id.codigo.qnocklib.controller;


import android.content.Context;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.codigo.qnocklib.config.Config;
import id.codigo.qnocklib.interfaces.CallbackInterface;
import id.codigo.qnocklib.interfaces.StatusInterface;
import id.codigo.qnocklib.interfaces.SubscribeInterface;
import id.codigo.qnocklib.interfaces.TokenInterface;
import id.codigo.qnocklib.interfaces.UnsubscribeInterface;
import id.codigo.qnocklib.model.CallbackAndroid;
import id.codigo.qnocklib.model.GetStatus;
import id.codigo.qnocklib.model.SubscribeChannel;
import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.qnocklib.model.UnsubscribeChannel;
import id.codigo.qnocklib.services.CallbackServices;
import id.codigo.qnocklib.services.StatusServices;
import id.codigo.qnocklib.services.SubsServices;
import id.codigo.qnocklib.services.TokenService;
import id.codigo.qnocklib.services.UnsubsServices;
import id.codigo.seedroid.helper.PreferenceHelper;
import id.codigo.seedroid.service.ServiceListener;

import static id.codigo.qnocklib.config.Config.token1;
import static id.codigo.qnocklib.utils.Util.getDateNow;

/**
 * Created by Codigo on 10/5/2017.
 */

public class Qnock {

    int environment;
    String URL_BASE = "";

    public static int DEV = 1;
    public static int STG = 2;
    public static int PRD = 3;





    /* public void getToken(final TokenInterface tokenInterface, String tokenId,
                      String message, String alert, String body, String secret, String id, String tokenCode, final Context context){
     new TokenServices().tokenGenerate(new ServiceListener<TokenGenerate>() {
         @Override
         public void onSuccess(TokenGenerate tokenGenerate) {
             if (tokenGenerate.getSTATUS().equals("200")) {
                 PreferenceHelper.getInstance().saveSession(Config.tokenDate, getDateNow());
                 PreferenceHelper.getInstance().saveSession(token1, tokenGenerate.getDATA());

                 tokenInterface.onTokenGet(true, "success", tokenGenerate.getDATA());

                 Intent intent = new Intent(Config.QNOCK_TOKEN);
                 intent.putExtra("token", tokenGenerate.getDATA());
                 LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
             }else {
                 tokenInterface.onTokenGet(false, "failed", null);
             }
         }

         @Override
         public void onFailed(String s) {
             tokenInterface.onTokenGet(false, s, null);
         }
     }, tokenId, message, alert, body, secret, id, tokenCode);
 }*/
//    public void getToken(final TokenInterface tokenInterface, String userId, String appsecret) {
//        new TokenService().tokenGenerate(new ServiceListener<TokenGenerate>() {
//            @Override
//            public void onSuccess(TokenGenerate tokenGenerate) {
//                if (tokenGenerate.getStatus().equals("200")) {
//                    PreferenceHelper.getInstance().saveSession(Config.tokenDate, getDateNow());
//                    PreferenceHelper.getInstance().saveSession(token1, tokenGenerate.getDisplay_message());
//
//                    tokenInterface.onTokenGet(true, "success", tokenGenerate);
//                    Log.e("TOKEN SERVICE", tokenGenerate.getDisplay_message());
//                } else {
//                    tokenInterface.onTokenGet(false, "failed", null);
//                    Log.e("TOKEN SERVICE", "NULL " + tokenGenerate.getStatus() + " - " + tokenGenerate.getMessage());
//
//                }
//            }
//
//            @Override
//            public void onFailed(String s) {
//                tokenInterface.onTokenGet(false, s, null);
//                Log.e("TOKEN SERVICE", "NULL " + s);
//
//            }
//        },userId, appsecret);
//    }
//
//    public void getStatus(final StatusInterface statusInterface, String appStatus, String token) {
//        new StatusServices().status(new ServiceListener<GetStatus>() {
//            @Override
//            public void onSuccess(GetStatus getStatus) {
//                if (getStatus.getStatus().equals("200")) {
//                    statusInterface.onStatusGet(true, "success", getStatus);
//                } else {
//                    statusInterface.onStatusGet(false, "failed", null);
//                }
//            }
//
//            @Override
//            public void onFailed(String s) {
//                statusInterface.onStatusGet(false, s, null);
//            }
//        }, appStatus, token);
//    }

   /* public void sendImpression(final CallbackInterface callbackInterface, final String secret, final String token) {
        if (compareDate()) {
            TokenInterface tokenInterface = new TokenInterface() {
                @Override
                public void onTokenGet(boolean status, String msg, TokenGenerate tokenObject) {
                    sendImpressionServer(callbackInterface, secret, token);
                    setStatus("on");
                }
            };
//            getToken(tokenInterface, tokenId, message, alert, body, secret, id, tokenCode, context);

        } else {
            sendImpressionServer(callbackInterface, secret, token);
            setStatus("off");
        }
    }*/

    /*private void sendImpressionServer(final CallbackInterface callbackInterface, String secret, String token) {
        new CallbackServices().androidCallback(new ServiceListener<CallbackAndroid>() {
            @Override
            public void onSuccess(CallbackAndroid callbackAndroid) {
                if (callbackAndroid.equals("success")) {
                    callbackInterface.onCallbackGet(true, "QNOCK Impression : Success", callbackAndroid);
                } else {
                    callbackInterface.onCallbackGet(false, "QNOCK Impression : Failed", null);
                }
            }

            @Override
            public void onFailed(String s) {
                callbackInterface.onCallbackGet(false, s, null);
            }
        }, secret, token);
    }*/

//    private void subscribeChannelServer(final SubscribeInterface subscribeInterface, String tokenId, String channel, String device, String token) {
//        new SubsServices().subscribeUser(new ServiceListener<SubscribeChannel>() {
//            @Override
//            public void onSuccess(SubscribeChannel subscribeChannel) {
//                if (subscribeChannel.getDATA().equals("200")) {
//                    subscribeInterface.onSubscribeGet(true, "QNOCK Subscribe Channel : Success", subscribeChannel.toString());
//                } else {
//                    subscribeInterface.onSubscribeGet(false, "QNOCK Subscribe Channel : Failed", null);
//                }
//            }
//
//            @Override
//            public void onFailed(String s) {
//                subscribeInterface.onSubscribeGet(false, s, null);
//            }
//        }, tokenId, channel, device, token);
//    }

    private void subscribeChannelServer(final SubscribeInterface subscribeInterface, JSONObject params) {
        new SubsServices().subscribeUser(new ServiceListener<SubscribeChannel>() {
            @Override
            public void onSuccess(SubscribeChannel subscribeChannel) {
                if (subscribeChannel.getDisplay_message().equals("200")) {
                    subscribeInterface.onSubscribeGet(true, "QNOCK Subscribe Channel : Success", subscribeChannel);
                } else {
                    subscribeInterface.onSubscribeGet(false, "QNOCK Subscribe Channel : Failed", null);
                }
            }

            @Override
            public void onFailed(String s) {
                subscribeInterface.onSubscribeGet(false, s, null);
            }
        }, params);
    }

    public void subscribeChannel(final SubscribeInterface subscribeInterface, final String tokenId,
                                 final String channel, final String device) {

        final String token = getTokenSaved();

        JSONObject json = new JSONObject();
        try {
            json.put("token", token);
            json.put("user_token_id", tokenId);
            json.put("channel", channel);
            json.put("device", "Android");
            if (!tokenId.equals("") || tokenId.length() != 0) {
                json.put("user_id", tokenId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //SubscribeInterface subscribeInterface1 = new SubscribeInterface() {
        //  @Override
        //public void onSubscribeGet(boolean status, String msg, String response) {
        //subscribeChannelServer(subscribeInterface, tokenId, channel, device, token);
        //}
        //};
        subscribeChannelServer(subscribeInterface, json);
    }

    public void subscribeChannel(final SubscribeInterface subscribeInterface, final String tokenId, final List<String> channel, final String device, final String token) {
        final String token1 = getTokenSaved();
        JSONObject json = new JSONObject();
        try {
            json.put("token", token1);
            json.put("user_token_id", tokenId);
            if (channel.size() > 1) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < channel.size(); i++) {
                    jsonArray.put(i, channel.get(i));
                }
                json.put("channel", jsonArray);
            } else {
                json.put("channel", channel.get(0));
            }
            if (!tokenId.equals("") || tokenId.length() != 0) {
                json.put("user_id", tokenId);
            }
            json.put("device", "Android");
            Log.d("JSON", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
            /*SubscribeInterface subscribeInterface1 = new SubscribeInterface() {
                @Override
                public void onSubscribeGet(boolean status, String msg, String response) {
                    subscribeChannelServer(subscribeInterface, tokenId, String.valueOf(channel), device, token);
                }
            };*/
        subscribeChannelServer(subscribeInterface, json);
    }

//    public void unsubscribeChannelServer(final UnsubscribeInterface unsubscribeInterface, String secret, String tokenId, String channel, String token) {
//        new UnsubsServices().unsubscribeUser(new ServiceListener<UnsubscribeChannel>() {
//            @Override
//            public void onSuccess(UnsubscribeChannel unsubscribeChannel) {
//                if (unsubscribeChannel.getDATA().equals("200")) {
//                    unsubscribeInterface.onUnsubscribeGet(true, "QNOCK Unsubscribe Channel : Success", unsubscribeChannel.toString());
//                } else {
//                    unsubscribeInterface.onUnsubscribeGet(false, "QNOCK Unsubscribe Channel : Failed", null);
//                }
//            }
//
//            @Override
//            public void onFailed(String s) {
//                unsubscribeInterface.onUnsubscribeGet(false, s, null);
//            }
//        }, secret, tokenId, channel, token);
//    }

    public void unsubscribeChannelServer(final UnsubscribeInterface unsubscribeInterface, JSONObject params) {
        new UnsubsServices().unsubscribeUser(new ServiceListener<UnsubscribeChannel>() {
            @Override
            public void onSuccess(UnsubscribeChannel unsubscribeChannel) {
                if (unsubscribeChannel.getDisplay_message().equals("200")) {
                    unsubscribeInterface.onUnsubscribeGet(true, "QNOCK Unsubscribe Channel : Success", unsubscribeChannel);
                } else {
                    unsubscribeInterface.onUnsubscribeGet(false, "QNOCK Unsubscribe Channel : Failed", null);
                }
            }

            @Override
            public void onFailed(String s) {
                unsubscribeInterface.onUnsubscribeGet(false, s, null);

            }
        }, params);
    }

    public void unsubscribeChannel(final UnsubscribeInterface unsubscribeInterface, final String secret,
                                   final String tokenId, final String channel, final String token) {
        final String token1 = getTokenSaved();

        JSONObject json = new JSONObject();
        try {
            json.put("token", token1);
            json.put("user_token_id", tokenId);
            json.put("channel", channel);
            Log.d("JSON", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        unsubscribeChannelServer(unsubscribeInterface, json);
    }

    public void unsubscribeChannel(final UnsubscribeInterface unsubscribeInterface, final String secret, final String tokenId, final List<String> channel, final String token) {
        final String token1 = getTokenSaved();

        JSONObject json = new JSONObject();
        try {
            json.put("token", token1);
            json.put("user_token_id", tokenId);
            if (channel.size() > 1) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < channel.size(); i++) {
                    jsonArray.put(i, channel.get(i));
                }
                json.put("channel", jsonArray);
            } else {
                json.put("channel", channel.get(0));
            }
            Log.d("JSON", json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        unsubscribeChannelServer(unsubscribeInterface, json);
    }

    public String getTokenSaved() {
        return PreferenceHelper.getInstance().getSessionString(Config.token1);
    }

    public void setStatus(String s) {
        if (s.equals("on")) {
            getStatus();
        } else {
            getStatus1();
        }
    }

    public String getStatus() {
        return "on";
    }

    public String getStatus1() {
        return "off";
    }

    private boolean compareDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        PreferenceHelper ph = new PreferenceHelper();
        Date strDate = null, strDateNow = null;
        try {
            strDate = sdf.parse(ph.getSessionString(Config.token1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (strDateNow.after(strDate)) {
            return true;
        } else {
            return false;
        }
    }

    private void setUrlBase(int environment) {
        if (environment == 1) {
            URL_BASE = "http://push.qnock.netconnect.dev.codigo.id";
        } else if (environment == 2) {
            URL_BASE = "http://push.qnock.netconnect.stg.codigo.id";
        } else if (environment == 3) {
            URL_BASE = "http://push.qnock.codigo.id";
        }
    }

    public void setEnvironment(int environment) {
        this.environment = environment;
        setUrlBase(environment);
    }
}
