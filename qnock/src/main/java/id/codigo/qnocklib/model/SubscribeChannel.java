package id.codigo.qnocklib.model;

import java.util.ArrayList;

/**
 * Created by Codigo on 9/28/2017.
 */

public class SubscribeChannel extends BaseModel {
    private String status;
    private String message;
    private Display_message display_message;
    private String time;

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Display_message getDisplay_message() {
        return display_message;
    }

    public void setDisplay_message(Display_message display_message) {
        this.display_message = display_message;
    }

    @Override
    public String getTime() {
        return time;
    }

    @Override
    public void setTime(String time) {
        this.time = time;
    }
}

