package id.codigo.qnocklib.model;

/**
 * Created by Codigo on 9/28/2017.
 */

public class CallbackAndroid{
    private String STATUS;
    private String DATA;
    private String TIME;

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getDATA() {
        return DATA;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    public String getTIME() {
        return TIME;
    }

    public void setTIME(String TIME) {
        this.TIME = TIME;
    }
}
