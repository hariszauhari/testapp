package id.codigo.qnocklib.model;

/**
 * Created by Codigo on 9/28/2017.
 */

public class GetStatus extends BaseModel {
//    private String data;
//
//    public String getDATA() {
//        return data;
//    }
//
//    public void setDATA(String data) {
//        this.data = data;
//    }
    private String status;
    private String data;
    private String message;

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
