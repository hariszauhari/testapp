package id.codigo.qnocklib.interfaces;

import id.codigo.qnocklib.model.TokenGenerate;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface TokenInterface {
    //void onTokenGet(boolean status, String msg, String token);
    void onTokenGet(boolean status, String msg, TokenGenerate tokenObject);
}
