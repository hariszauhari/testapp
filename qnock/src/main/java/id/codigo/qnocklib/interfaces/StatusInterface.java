package id.codigo.qnocklib.interfaces;

import id.codigo.qnocklib.model.GetStatus;
import id.codigo.qnocklib.services.StatusServices;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface StatusInterface {
    void onStatusGet(boolean status, String msg, GetStatus getStatus);
}
