package id.codigo.qnocklib.interfaces;

import id.codigo.qnocklib.model.SubscribeChannel;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface SubscribeInterface {
    void onSubscribeGet(boolean status, String msg, SubscribeChannel subscribeChannel);
}
