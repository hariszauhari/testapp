package id.codigo.qnocklib.interfaces;

import id.codigo.qnocklib.model.UnsubscribeChannel;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface UnsubscribeInterface {
    void onUnsubscribeGet(boolean status, String msg, UnsubscribeChannel unsubscribeChannel);
}
