package id.codigo.qnocklib.interfaces;

import id.codigo.qnocklib.model.CallbackAndroid;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface CallbackInterface {
    void onCallbackGet(boolean status, String msg, CallbackAndroid callbackObject);
}
