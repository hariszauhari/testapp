package id.codigo.qnocklib.services;


import android.util.Base64;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.model.SubscribeChannel;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.helper.LogHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/29/2017.
 */

public class SubsServices {
    public void subscribeUser (ServiceListener <SubscribeChannel> callback, String tokenId, String channel, String device, String token){
        String url = QnockConfig.URL_BASE + "/user/subscribe";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> subscribe = new HashMap<>();
        subscribe.put("user_token_id", tokenId);
        subscribe.put("channel", channel);
        subscribe.put("device", device);
        subscribe.put("token", token);
        HttpHelper.getInstance().post(url, subscribe, callback);
    }
    public void subscribeUser (ServiceListener <SubscribeChannel> callback, JSONObject payload){
        String url = RestConfigs.rootUrl + "/user/subscribe";

        String credentials = QnockConfig.appId + ":" + QnockConfig.appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", auth);
        header.put("Content-Type", "application/json; charset=utf-8");

        Map<String,String> map = new HashMap<>();

        HttpHelper.getInstance().post(url,header,map, payload, callback);

    }
}
