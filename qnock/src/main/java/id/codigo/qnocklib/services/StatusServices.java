package id.codigo.qnocklib.services;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.model.GetStatus;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 10/12/2017.
 */

public class StatusServices {
    public void status (ServiceListener<GetStatus> callback, String token){
        String url = QnockConfig.URL_BASE + "/apps/status";
        String credentials = QnockConfig.appId + ":" + QnockConfig.appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT);

        Map<String, String> header = new HashMap<>();
        header.put("Authorization", auth);
        header.put("Content-Type", "application/json; charset=utf-8");

        JSONObject json = new JSONObject();
        try {
            json.put("app_secret", QnockConfig.appSecret);
            json.put("token", token);
            Log.d("", "JSON : " + json.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpHelper.getInstance().post(url, header, json, callback);

    }
}
