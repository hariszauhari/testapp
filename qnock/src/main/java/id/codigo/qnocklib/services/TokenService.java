package id.codigo.qnocklib.services;

import android.util.Base64;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 10/12/2017.
 */

public class TokenService {
    public void tokenGenerate(ServiceListener<TokenGenerate> callback){
        String url = QnockConfig.URL_BASE + "/token";
        Map<String, String> param = new HashMap<>();

        String credentials = QnockConfig.appId + ":" + QnockConfig.appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT);
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", auth);
        header.put("Content-Type", "application/json; charset=utf-8");

        HttpHelper.getInstance().post(url, header, param, callback);
    }

}
