package id.codigo.qnocklib.services;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.controller.Qnock2;
import id.codigo.qnocklib.model.CallbackAndroid;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

import static android.content.ContentValues.TAG;

/**
 * Created by Codigo on 9/29/2017.
 */

public class CallbackServices {
    public void androidCallback (ServiceListener <CallbackAndroid> callback, JSONObject params) {
        String url = RestConfigs.rootUrl + "/callback/android";

        String credentials = QnockConfig.appId + ":" + QnockConfig.appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(),Base64.DEFAULT);

        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", auth);
        header.put("Content-Type", "application/json; charset=utf-8");

        Map<String, String> map = new HashMap<>();

        HttpHelper.getInstance().post(url, header, map,  params , callback);

    }
}