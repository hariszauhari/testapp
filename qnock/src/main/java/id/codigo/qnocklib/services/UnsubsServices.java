package id.codigo.qnocklib.services;

import android.util.Base64;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.config.QnockConfig;
import id.codigo.qnocklib.model.SubscribeChannel;
import id.codigo.qnocklib.model.UnsubscribeChannel;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/29/2017.
 */

public class UnsubsServices {
    public void unsubscribeUser (ServiceListener <UnsubscribeChannel> callback, String secret, String tokenId, String channel, String token){
        String url = RestConfigs.rootUrl + "/user/unsubscribe";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> unsubs = new HashMap<>();
        unsubs.put("app_secret", secret);
        unsubs.put("user_token_id", tokenId);
        unsubs.put("channel", channel);
        unsubs.put("token", token);
        HttpHelper.getInstance().post(url, unsubs, callback);
    }
    public void unsubscribeUser (ServiceListener <UnsubscribeChannel> callback, JSONObject params){
        String url = RestConfigs.rootUrl + "/user/subscribe";

        String credentials = QnockConfig.appId + ":" + QnockConfig.appSecret;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", auth);
        header.put("Content-Type", "application/json; charset=utf-8");

        Map<String,String> map = new HashMap<>();

        HttpHelper.getInstance().post(url, map, params, callback);
    }
}
